# Portofolio Angular Project + Mobile Hybrid

<br>
Undiknas Web Fakultas<br>
Url: http://feb.undiknas.ac.id<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undinas-fakultas/1.jpg"><br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undinas-fakultas/2.jpg"><br>
<br>
<br>
Undiknas Admission<br>
Url: https://admission.undiknas.ac.id<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-admission/2.jpg"><br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-admission/1.jpg"><br>
<br>
<br>
Undiknas Cloud<br>
Url: https://cloud.undiknas.ac.id<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/1.jpg"><br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/2.jpg"><br>
<br>
<br>

# Integrated System

Undiknas Cloud Perpustakan<br>
Url: https://cloud.undiknas.ac.id/perpustakaan<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/perpus.jpg"><br>
<br>
<br>
Undiknas Cloud Penunjang Akademik<br>
Url: https://cloud.undiknas.ac.id/ppa<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/ppa.jpg"><br>
<br>
<br>
Undiknas Cloud Keuangan<br>
Url: https://cloud.undiknas.ac.id/keuangan<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/keuangan.jpg"><br>
<br>
<br>
Undiknas Cloud Prodi<br>
Url: https://cloud.undiknas.ac.id/prodi<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/prodi.jpg"><br>
<br>
<br>
Undiknas Cloud Personalia<br>
Url: https://cloud.undiknas.ac.id/personalia<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-cloud/personalia.jpg"><br>
<br>
<br>

# Mobile App
Undiknas Mobile<br>
Url: https://play.google.com/store/apps/details?id=com.cyber.undiknas<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-mobile/1.jpg">
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-mobile/2.jpg">
<br>
<br>
Undiknas Pasca Mobile<br>
Url: https://play.google.com/store/apps/details?id=com.kodelapan.ugsmobile<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/ugs-mobile/1.jpg">
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/ugs-mobile/2.jpg">
<br>
<br>
Undiknas Web Library<br>
Url: http://library.undiknas.ac.id<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-library/1.jpg"><br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-library/2.jpg"><br>
<br>
<br>
Undiknas Web SPM<br>
Url: http://spm.undiknas.ac.id<br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-spm/1.jpg"><br>
<img src="https://gitlab.com/bhiantara/portofolio/raw/master/undiknas-spm/2.jpg"><br>
<br>
<br>
